网站功能开发
-
本源码引入PSI的开源代码，基于PSI上面进行二次开发，添加许多功能<br/>
PSI作者源码：https://git.oschina.net/crm8000/PSI<br />
数据库目录：data\erp.sql 直接导入这一个sql文件即可

网站功能修改日志
-
>2017年1月3日 新增商品-选择品牌添加搜索功能已修复<br/>
>2017年1月12日 新增库存月报表<br/>
>2017年1月18日 新增采购需求<br/>
>2017年1月18日 客户资料新增邮箱字段<br/>
>2017年2月8日 客户资料新增业务员字段,修复若干功能<br/>
>2017年2月24日 业务日志修复,若干功能修复<br/>
>2017年3月6日 修复新增商品如果没有设置品牌采购订单搜索不到的问题，去除了数据域的功能<br/>
>2017年3月28日 若干bug,添加打印备注功能，采购入库新增入库数量和未入库数量，入库数量为真实要入库的数量<br/>
>2017年4月18日 修复了打印功能，添加打印备注，打印备注可保存，若干bug<br/>
>2017年5月22日 添加官方项目的功能（“单据生成PDF”，采购订单，销售订单，拆分入库出库的功能）（采购入库新增入库数量和未入库数量，入库数量为真实要入库的数量，取消掉了，感觉官方的更好用些）<br/>
>2017年6月12日新增官方项目功能“价格体系”<br/>
>2017年11月1日 修复之前余留的一些小问题，现在长期使用没有什么bug