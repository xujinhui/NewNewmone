Ext.define("PSI.Const", {
	statics : {
		REQUIRED : '<span style="color:red;font-weight:bold" data-qtip="必录项">*</span>',
		LOADING : "数据加载中...",
		SAVING : "数据保存中...",
		BASE_URL : "",
		VERSION : "PSI 2016 - build201611150855",
		FORMAT_MONEY : "0,000.000", //numbercolumn框的数字格式化
		NUMBER_FIELD : 3 //numberfield编辑框的保留小数
	}
});
