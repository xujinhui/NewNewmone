/**
 * 销售月报表(按客户汇总)
 */
Ext.define("PSI.Report.SaleMonthByCustomerForm", {
	extend : "Ext.panel.Panel",

	border : 0,

	layout : "border",

	initComponent : function() {
		var me = this;

		Ext.apply(me, {
					tbar : [{
								text : "关闭",
								iconCls : "PSI-button-exit",
								handler : function() {
									location.replace(PSI.Const.BASE_URL);
								}
							}],
					items : [{
						region : "north",
						height : 60,
						border : 0,
						layout : "fit",
						border : 1,
						title : "查询条件",
						collapsible : true,
						layout : {
							type : "table",
							columns : 4
						},
						items : [{
									id : "editQueryYear",
									xtype : "numberfield",
									margin : "5, 0, 0, 0",
									labelAlign : "right",
									labelSeparator : "",
									fieldLabel : "年",
									labelWidth : 20,
									width : 100,
									value : (new Date()).getFullYear()
								}, {
									id : "editQueryMonth",
									xtype : "combobox",
									margin : "5, 0, 0, 0",
									labelAlign : "right",
									labelSeparator : "",
									labelWidth : 20,
									fieldLabel : " ",
									store : Ext.create("Ext.data.ArrayStore", {
												fields : ["id", "text"],
												data : [[1, "一月"], [2, "二月"],
														[3, "三月"], [4, "四月"],
														[5, "五月"], [6, "六月"],
														[7, "七月"], [8, "八月"],
														[9, "九月"], [10, "十月"],
														[11, "十一月"],
														[12, "十二月"]]
											}),
									valueField : "id",
									displayFIeld : "text",
									queryMode : "local",
									editable : false,
									value : (new Date()).getMonth() + 1,
									width : 90
								}, {
									xtype : "container",
									items : [{
												xtype : "button",
												text : "查询",
												width : 100,
												margin : "5 0 0 10",
												iconCls : "PSI-button-refresh",
												handler : me.onQuery,
												scope : me
											}, {
												xtype : "button",
												text : "重置查询条件",
												width : 100,
												margin : "5, 0, 0, 10",
												handler : me.onClearQuery,
												scope : me
											}]
								}]
					}, {
						region : "center",
						layout : "border",
						border : 0,
						items : [{
									region : "center",
									layout : "fit",
									items : [me.getMainGrid()]
								}, {
									region : "east",
									layout : "fit",
									split : true,
									width : 600,
									items : [me.getRvDetailGrid()]
								}, {
									region : "south",
									layout : "fit",
									height : 100,
									items : [me.getSummaryGrid()]
								}]
					}]
				});

		me.callParent(arguments);
	},

	//获取业务单据id
	getRvParam : function() {
		var item = this.getMainGrid().getSelectionModel().getSelection();
		if (item == null || item.length != 1) {
			return null;
		}

		var rv = item[0];
		return rv.get("customerId");
	},

	onRvGridSelect : function() {
		this.getRvDetailGrid().getStore().loadPage(1);
	},

	getRvDetailGrid : function(){
		var me = this;
		if (me.__rvDetailGrid) {
			return me.__rvDetailGrid;
		}

		Ext.define("PSIRvDetail", {
			extend : "Ext.data.Model",
			fields : ["id", "rvMoney", "actMoney", "balanceMoney",
				"refType", "refNumber", "bizDT", "dateCreated"]
		});

		var store = Ext.create("Ext.data.Store", {
			model : "PSIRvDetail",
			pageSize : 20,
			proxy : {
				type : "ajax",
				actionMethods : {
					read : "POST"
				},
				url : PSI.Const.BASE_URL + "Home/Funds/rvDetailList",
				reader : {
					root : 'dataList',
					totalProperty : 'totalCount'
				}
			},
			autoLoad : false,
			data : []
		});

		store.on("beforeload", function() {
			Ext.apply(store.proxy.extraParams, {
				caType : "customer",
				caId : me.getRvParam()
			});
		});

		me.__rvDetailGrid = Ext.create("Ext.grid.Panel", {
			viewConfig : {
				enableTextSelection : true
			},
			border : 0,
			tbar : [{
				border:0,
				xtype : "pagingtoolbar",
				store : store
			}],
			columnLines : true,
			columns : [{
				header : "业务类型",
				dataIndex : "refType",
				menuDisabled : true,
				sortable : false,
				width : 120
			}, {
				header : "单号",
				dataIndex : "refNumber",
				menuDisabled : true,
				sortable : false,
				width : 120,
				renderer : function(value, md, record) {
					if (record.get("refType") == "应收账款期初建账") {
						return value;
					}

					return "<a href='"
						+ PSI.Const.BASE_URL
						+ "Home/Bill/viewIndex?fid=2004&refType="
						+ encodeURIComponent(record
							.get("refType"))
						+ "&ref="
						+ encodeURIComponent(record
							.get("refNumber"))
						+ "' target='_blank'>" + value
						+ "</a>";
				}
			}, {
				header : "业务日期",
				dataIndex : "bizDT",
				menuDisabled : true,
				sortable : false
			}, {
				header : "应收金额",
				dataIndex : "rvMoney",
				menuDisabled : true,
				sortable : false,
				align : "right",
				format: PSI.Const.FORMAT_MONEY,
				xtype : "numbercolumn"
			}, {
				header : "已收金额",
				dataIndex : "actMoney",
				menuDisabled : true,
				sortable : false,
				align : "right",
				format: PSI.Const.FORMAT_MONEY,
				xtype : "numbercolumn"
			}, {
				header : "未收金额",
				dataIndex : "balanceMoney",
				menuDisabled : true,
				sortable : false,
				align : "right",
				format: PSI.Const.FORMAT_MONEY,
				xtype : "numbercolumn"
			}, {
				header : "创建时间",
				dataIndex : "dateCreated",
				menuDisabled : true,
				sortable : false,
				width : 140
			}],
			store : store,
			listeners : {
				select : {
					fn : me.onRvDetailGridSelect,
					scope : me
				}
			}
		});

		return me.__rvDetailGrid;
	},

	getMainGrid : function() {
		var me = this;
		if (me.__mainGrid) {
			return me.__mainGrid;
		}

		var modelName = "PSIReportSaleMonthByCustomer";
		Ext.define(modelName, {
					extend : "Ext.data.Model",
					fields : ["bizDT", "customerCode", "customerName",
							"saleMoney", "rejMoney", "m", "profit", "rate","customerId"]
				});
		var store = Ext.create("Ext.data.Store", {
					autoLoad : false,
					model : modelName,
					data : [],
					pageSize : 20,
					proxy : {
						type : "ajax",
						actionMethods : {
							read : "POST"
						},
						url : PSI.Const.BASE_URL
								+ "Home/Report/saleMonthByCustomerQueryData",
						reader : {
							root : 'dataList',
							totalProperty : 'totalCount'
						}
					}
				});
		store.on("beforeload", function() {
					store.proxy.extraParams = me.getQueryParam();
				});

		me.__mainGrid = Ext.create("Ext.grid.Panel", {
					viewConfig : {
						enableTextSelection : true
					},
					border : 0,
					columnLines : true,
					columns : [{
								xtype : "rownumberer"
							}, {
								header : "月份",
								dataIndex : "bizDT",
								menuDisabled : true,
								sortable : false,
								width : 80
							}, {
								header : "客户编码",
								dataIndex : "customerCode",
								menuDisabled : true,
								sortable : false
							}, {
								header : "客户",
								dataIndex : "customerName",
								menuDisabled : true,
								sortable : false,
								width : 200
							}, {
								header : "销售出库金额",
								dataIndex : "saleMoney",
								menuDisabled : true,
								sortable : false,
								align : "right",
						format: PSI.Const.FORMAT_MONEY,
								xtype : "numbercolumn"
							}, {
								header : "退货入库金额",
								dataIndex : "rejMoney",
								menuDisabled : true,
								sortable : false,
								align : "right",
						format: PSI.Const.FORMAT_MONEY,
								xtype : "numbercolumn"
							}, {
								header : "净销售金额",
								dataIndex : "m",
								menuDisabled : true,
								sortable : false,
								align : "right",
						format: PSI.Const.FORMAT_MONEY,
								xtype : "numbercolumn"
							}, {
								header : "毛利",
								dataIndex : "profit",
								menuDisabled : true,
								sortable : false,
								align : "right",
						format: PSI.Const.FORMAT_MONEY,
								xtype : "numbercolumn"
							}, {
								header : "毛利率",
								dataIndex : "rate",
								menuDisabled : true,
								sortable : false,
								align : "right"
							}],
					store : store,
					tbar : [{
								id : "pagingToobar",
								xtype : "pagingtoolbar",
								border : 0,
								store : store
							}, "-", {
								xtype : "displayfield",
								value : "每页显示"
							}, {
								id : "comboCountPerPage",
								xtype : "combobox",
								editable : false,
								width : 60,
								store : Ext.create("Ext.data.ArrayStore", {
											fields : ["text"],
											data : [["20"], ["50"], ["100"],
													["300"], ["1000"]]
										}),
								value : 20,
								listeners : {
									change : {
										fn : function() {
											store.pageSize = Ext
													.getCmp("comboCountPerPage")
													.getValue();
											store.currentPage = 1;
											Ext.getCmp("pagingToobar")
													.doRefresh();
										},
										scope : me
									}
								}
							}, {
								xtype : "displayfield",
								value : "条记录"
							}],
					listeners : {
						select : {
							fn : me.onRvGridSelect,
							scope : me
						}
					}
				});

		return me.__mainGrid;
	},

	getSummaryGrid : function() {
		var me = this;
		if (me.__summaryGrid) {
			return me.__summaryGrid;
		}

		var modelName = "PSIReportSaleMonthByCustomerSummary";
		Ext.define(modelName, {
					extend : "Ext.data.Model",
					fields : ["bizDT", "saleMoney", "rejMoney", "m", "profit",
							"rate"]
				});
		var store = Ext.create("Ext.data.Store", {
					autoLoad : false,
					model : modelName,
					data : []
				});

		me.__summaryGrid = Ext.create("Ext.grid.Panel", {
					title : "月销售汇总",
					viewConfig : {
						enableTextSelection : true
					},
					border : 0,
					columnLines : true,
					columns : [{
								header : "月份",
								dataIndex : "bizDT",
								menuDisabled : true,
								sortable : false,
								width : 80
							}, {
								header : "销售出库金额",
								dataIndex : "saleMoney",
								menuDisabled : true,
								sortable : false,
								align : "right",
						format: PSI.Const.FORMAT_MONEY,
								xtype : "numbercolumn"
							}, {
								header : "退货入库金额",
								dataIndex : "rejMoney",
								menuDisabled : true,
								sortable : false,
								align : "right",
						format: PSI.Const.FORMAT_MONEY,
								xtype : "numbercolumn"
							}, {
								header : "净销售金额",
								dataIndex : "m",
								menuDisabled : true,
								sortable : false,
								align : "right",
						format: PSI.Const.FORMAT_MONEY,
								xtype : "numbercolumn"
							}, {
								header : "毛利",
								dataIndex : "profit",
								menuDisabled : true,
								sortable : false,
								align : "right",
						format: PSI.Const.FORMAT_MONEY,
								xtype : "numbercolumn"
							}, {
								header : "毛利率",
								dataIndex : "rate",
								menuDisabled : true,
								sortable : false,
								align : "right"
							}],
					store : store
				});

		return me.__summaryGrid;
	},

	onQuery : function() {
		this.refreshMainGrid();
		this.refreshSummaryGrid();
	},

	refreshSummaryGrid : function() {
		var me = this;
		var grid = me.getSummaryGrid();
		var el = grid.getEl() || Ext.getBody();
		el.mask(PSI.Const.LOADING);
		Ext.Ajax.request({
					url : PSI.Const.BASE_URL
							+ "Home/Report/saleMonthByCustomerSummaryQueryData",
					params : me.getQueryParam(),
					method : "POST",
					callback : function(options, success, response) {
						var store = grid.getStore();

						store.removeAll();

						if (success) {
							var data = Ext.JSON.decode(response.responseText);
							store.add(data);
						}

						el.unmask();
					}
				});
	},

	onClearQuery : function() {
		var me = this;

		Ext.getCmp("editQueryYear").setValue((new Date()).getFullYear());
		Ext.getCmp("editQueryMonth").setValue((new Date()).getMonth() + 1);

		me.onQuery();
	},

	getQueryParam : function() {
		var me = this;

		var result = {};

		var year = Ext.getCmp("editQueryYear").getValue();
		if (year) {
			result.year = year;
		} else {
			year = (new Date()).getFullYear();
			Ext.getCmp("editQueryYear").setValue(year);
			result.year = year;
		}

		var month = Ext.getCmp("editQueryMonth").getValue();
		if (month) {
			result.month = month;
		} else {
			month = (new Date()).getMonth() + 1;
			Ext.getCmp("editQueryMonth").setValue(month);
			result.month = month;
		}

		return result;
	},

	refreshMainGrid : function(id) {
		this.getRvDetailGrid().getStore().removeAll();
		Ext.getCmp("pagingToobar").doRefresh();
	}
});