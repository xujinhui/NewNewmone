/**
 * 首页
 */
Ext.define("PSI.Home.MainForm", {
    extend : "Ext.panel.Panel",

    config : {
        pSale : "",
    },

    border : 0,
    bodyPadding : 5,

    getPortal : function(index) {
        var me = this;
        if (!me.__portalList) {
            me.__portalList = [];
            var pSale = me.getPSale() == "1";
            if (pSale) {
                me.__portalList.push(me.getSalePortal());
            }
        }

        if (index == 0 && me.__portalList.length == 0) {
            return me.getInfoPortal();
        }

        if (index >= me.__portalList.length || index < 0) {
            return {
                border : 0
            };
        }

        return me.__portalList[index];
    },

    initComponent : function() {
        var me = this;

        Ext.apply(me, {
            layout : "hbox",
            items : [{
                region : "west",
                flex : 1,
                layout : "vbox",
                border : 0,
                items : [me.getPortal(0),
                    me.getPortal(2)]
            }, {
                flex : 1,
                layout : "vbox",
                border : 0,
                items : [me.getPortal(1),
                    me.getPortal(3)]
            }]
        });

        me.callParent(arguments);

        me.querySaleData();
    },

    getSaleGrid : function() {
        var me = this;
        if (me.__saleGrid) {
            return me.__saleGrid;
        }

        var modelName = "PSIPortalSale";
        Ext.define(modelName, {
            extend : "Ext.data.Model",
            fields : ["month", "saleMoney", "profit", "rate"]
        });

        me.__saleGrid = Ext.create("Ext.grid.Panel", {
            viewConfig : {
                enableTextSelection : true
            },
            columnLines : true,
            border : 0,
            columns : [{
                header : "月份",
                dataIndex : "month",
                width : 80,
                menuDisabled : true,
                sortable : false
            }, {
                header : "销售额",
                dataIndex : "saleMoney",
                width : 120,
                menuDisabled : true,
                sortable : false,
                align : "right",
                xtype : "numbercolumn",
                format: PSI.Const.FORMAT_MONEY,
            }, {
                header : "毛利",
                dataIndex : "profit",
                width : 120,
                menuDisabled : true,
                sortable : false,
                align : "right",
                xtype : "numbercolumn",
                format: PSI.Const.FORMAT_MONEY,
            }, {
                header : "毛利率",
                dataIndex : "rate",
                menuDisabled : true,
                sortable : false,
                align : "right"
            }],
            store : Ext.create("Ext.data.Store", {
                model : modelName,
                autoLoad : false,
                data : []
            })
        });

        return me.__saleGrid;
    },

    getSalePortal : function() {
        var me = this;
        return {
            flex : 1,
            width : "100%",
            height : 240,
            margin : "5",
            header : {
                title : "<span style='font-size:120%'>销售看板</span>",
                iconCls : "PSI-portal-sale",
                height : 40
            },
            layout : "fit",
            items : [me.getSaleGrid()]
        };
    },

    queryInventoryData : function() {

    },

    querySaleData : function() {
        var me = this;
        var grid = me.getSaleGrid();
        var el = grid.getEl() || Ext.getBody();
        el.mask(PSI.Const.LOADING);
        Ext.Ajax.request({
            url : PSI.Const.BASE_URL + "Home/Portal/salePortal",
            method : "POST",
            callback : function(options, success, response) {
                var store = grid.getStore();
                store.removeAll();

                if (success) {
                    var data = Ext.JSON
                        .decode(response.responseText);
                    store.add(data);
                }

                el.unmask();
            }
        });
    },

    getInfoPortal : function() {
        return {
            border : 0,
            html : "<h1>欢迎使用牛魔王企业管理系统</h1>"
        }
    }
});