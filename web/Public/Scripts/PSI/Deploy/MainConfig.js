/**
 * 业务设置 - 主窗体
 */
Ext.define("PSI.BizConfig.MainConfig", {
    extend : "Ext.panel.Panel",

    initComponent : function() {
        var me = this;

        var modelName = "PSICompany";
        Ext.define(modelName, {
            extend : "Ext.data.Model",
            fields : ["id", "name"]
        });

        //设置
        Ext.apply(me, {
            border : 0, //设置间距
            layout : "border", //布局方式，border为整个面板  fit弹出框

            items : [{
                region : "center",
                layout : "fit",
                xtype : "panel",
                border : 0,
                items : [me.getGrid()]
            }]
        });

        //设置顶部面板
        me.callParent();

        me.queryCompany();
    },

    getGrid : function() {
        var me = this;
        if (me.__grid) {
            return me.__grid;
        }

        var modelName = "PSIBizConfig";
        Ext.define(modelName, {
            extend : "Ext.data.Model",
            fields : ["name", "age", "sex"],
            idProperty : "id"
        });
        var store = Ext.create("Ext.data.Store", {
            model : modelName,
            data : [],
            autoLoad : false
        });

        me.__grid = Ext.create("Ext.grid.Panel", {
            viewConfig : {
                enableTextSelection : true
            },
            loadMask : true,
            border : 0,
            columnLines : true,
            columns : [Ext.create("Ext.grid.RowNumberer", {
                text : "序号",
                width : 40
            }), {
                text : "姓名",
                dataIndex : "name",
                width : 250,
                menuDisabled : true
            }, {
                text : "年龄",
                dataIndex : "age",
                width : 250,
                menuDisabled : true
            }, {
                text : "性别",
                dataIndex : "sex",
                width : 250,
                menuDisabled : true
            }],
            store : store,
            listeners : {
                itemdblclick : {
                    fn : me.onEdit,
                    scope : me
                }
            }
        });

        return me.__grid;
    },

    /**
     * 刷新Grid数据
     */
    refreshGrid : function(id) {
        var me = this;
        var grid = me.getGrid();
        var el = grid.getEl() || Ext.getBody();
        el.mask(PSI.Const.LOADING);
        var store = grid.getStore();
        // store.removeAll();
        var data = Ext.JSON.decode("{name:'小帅',age:'18',sex:'男'}");
        store.add(data);

        console.log(el);
        console.log(grid);
        console.log(PSI.Const.LOADING);
        return;
    },



    /**
     * 查询公司信息
     */
    queryCompany : function() {
        var me = this;
        me.refreshGrid();
    },



});