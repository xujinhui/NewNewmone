/**
 * 商品图片导入
 *
 * @author 张健
 */
Ext.define("PSI.Goods.GoodsImportImageFrom", {
    extend : "Ext.window.Window",

    config : {
        parentForm : null,
        goodsId : null,
        goods:null
    },

    /**
     * 初始化组件
     */
    initComponent : function() {
        var me = this;

        var buttons = [];

        buttons.push({
            text : "导入商品图片",
            formBind : true,
            iconCls : "PSI-button-ok",
            handler : function() {
                me.onOK();
            },
            scope : me
        }, {
            text : "关闭",
            handler : function() {
                me.close();
            },
            scope : me
        });

        Ext.apply(me, {
            title : "导入商品图片",
            modal : true,
            resizable : false,
            onEsc : Ext.emptyFn,
            width : 512,
            height : 138,
            layout : "fit",
            items : [{
                id : "importForm",
                xtype : "form",
                layout : {
                    type : "table",
                    columns : 1
                },
                height : "100%",
                bodyPadding : 5,
                fieldDefaults : {
                    labelWidth : 60,
                    labelAlign : "right",
                    labelSeparator : "",
                    msgTarget : 'side'
                },
                items : [{
                    xtype: 'textfield',
                    name:'goodsId',
                    value:me.getGoodsId(),
                    hidden: true,
                    hideLabel:true,
                },{
                    xtype : 'textfield',
                    name : 'goodsName',
                    id : 'goodsName',
                    fieldLabel : '图片名称',
                    labelWidth : 50,
                    width : 480,
                    anchor : '100%',
                },{
                    xtype : 'filefield',
                    name : 'data_file',
                    afterLabelTextTpl : '<span style="color:red;font-weight:bold" data-qtip="必需填写">*</span>',
                    fieldLabel : '图片',
                    labelWidth : 50,
                    width : 480,
                    msgTarget : 'side',
                    allowBlank : false,
                    anchor : '100%',
                    buttonText : '选择商品图片'
                }],
                buttons : buttons
            }]
        });

        me.callParent(arguments);
    },

    onOK : function() {
        var me = this;
        var f = Ext.getCmp("importForm");
        var gn = Ext.getCmp("goodsName");
        var el = f.getEl();
        //图片名称

        if(gn.getValue().replace(/(^\s*)|(\s*$)/g, "") == ""){
            PSI.MsgBox.showInfo('请输入图片名称');
            return;
        }

        el.mask('正在导入...');
        f.submit({
            url : PSI.Const.BASE_URL + "Home/Goods/importImg",
            method : "POST",
            success : function(form, action) {
                el.unmask();

                PSI.MsgBox.showInfo("导入成功" + action.result.msg);
                me.focus();
                me.close();
                me.getParentForm().refresh_goods_images(me.getGoods());
            },
            failure : function(form, action) {
                el.unmask();
                PSI.MsgBox.showInfo("导入失败:" + action.result.msg);
            }
        });
    }
});