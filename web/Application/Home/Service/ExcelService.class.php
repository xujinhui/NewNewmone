<?php

namespace Home\Service;

require __DIR__ . '/../Common/Excel/PHPExcel/IOFactory.php';
require __DIR__ . '/../Common/Excel/PHPExcel.php';

use \PHPExcel_Style_Alignment;
use \PHPExcel_Style_Border;

/**
 * excel文件 Service
 *
 * @author cory
 */
class ExcelService extends PSIBaseExService {

    //默认字体
    private $typeface = '宋体';
    //默认字体大小
    private $typefaceSize = '11';
    
    /**
     * 按照公共的格式打印出Excel产品订单文档，此方法后不要输出任何数据
    $output_info = array(
        'title'=>'标题名称',
        'filename'=>'文件名称',
        'information'=>array(
            '单号：PD20170828001',
            '供应商：撒打算大大',
            '交货大健康代理商啊:dsakj dhskaj dhjksadhj ksa ',
        ),
        'goods'=>array(
            array(
                'goodsCode'=>'100001',
                'goodsName'=>'4寸红麻轮-铁心单面凹',
                'goodsSpec'=>'100*6*16mm',
                'goodsCount'=>'100.00',
                'unitName'=>'个',
                'goodsPrice'=>'9',
                'goodsMoney'=>'0.00',
            ),
            array(
                'goodsCode'=>'22100001',
                'goodsName'=>'222222222224寸红麻轮-铁心单面凹',
                'goodsSpec'=>'100*6*16mm',
                'goodsCount'=>'100.00',
                'unitName'=>'个',
                'goodsPrice'=>'11',
                'goodsMoney'=>'12',
            )
        ),
    );
     */
    public function output($output_info){

        $objPHPExcel = new \PHPExcel();
        //设置默认信息
        $objPHPExcel->getDefaultStyle()->getFont()->setName($this->typeface); //默认字体
        $objPHPExcel->getDefaultStyle()->getFont()->setSize($this->typefaceSize); //默认字体大小
        //------------标题 -----
        //合并第一行单元格
        $objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
        //添加数据
        $objPHPExcel->setActiveSheetIndex(0);
        //设置标题
        $objPHPExcel->getActiveSheet()->setCellValue('A1', $output_info['title']);
        //字体
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setName($this->typeface);
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
        //合并并居中
        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
            array(
                'font' => array (
                    'bold' => true
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
                )
            )
        );
        //垂直居中
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        //行高
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight('33.75');
        //------------标题结束 -----
        //------------文档整体 -----
        //设置列宽
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('12.88');
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('26.75');
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('21.38');
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('12.88');
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('12.88');
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('12.88');
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth('12.88');
        //------------整体结束 -----
        //------------设置资料 -----
        //设置资料
        $cell_index = 2; //指向行数索引
        foreach ($output_info['information'] as $key=>$value){
            $firse_cell = 'A'.($cell_index);
            $end_cell = 'G'.($cell_index);
            $objPHPExcel->getActiveSheet()->mergeCells($firse_cell.':'.$end_cell);
            $objPHPExcel->getActiveSheet()->setCellValue($firse_cell, $value);
            $objPHPExcel->getActiveSheet()->getRowDimension($cell_index)->setRowHeight('15.75');
            $cell_index++;
        }
        //------------资料结束 -----
        //设置商品菜单信息
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$cell_index, '商品编号');
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$cell_index, '商品名称');
        $objPHPExcel->getActiveSheet()->setCellValue('C'.$cell_index, '规格型号');
        $objPHPExcel->getActiveSheet()->setCellValue('D'.$cell_index, '数量');
        $objPHPExcel->getActiveSheet()->setCellValue('E'.$cell_index, '单位');
        $objPHPExcel->getActiveSheet()->setCellValue('F'.$cell_index, '采购单价');
        $objPHPExcel->getActiveSheet()->setCellValue('G'.$cell_index, '采购金额');
        //行高
        $objPHPExcel->getActiveSheet()->getRowDimension($cell_index)->setRowHeight('30.75');
        //设置垂直和水平居中
        $objPHPExcel->getActiveSheet()->getStyle('A'.$cell_index.':G'.$cell_index)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A'.$cell_index.':G'.$cell_index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $cell_index++;
        $goods_index = $cell_index;
        foreach ($output_info['goods'] as $key =>$value){
            //设置单条商品列表
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$cell_index, $value['goodsCode']);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$cell_index, $value['goodsName']);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$cell_index, $value['goodsSpec']);
            $objPHPExcel->getActiveSheet()->getStyle('D'.$cell_index)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$cell_index, $value['goodsCount']);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$cell_index, $value['unitName']);
            //设置为小数
            $objPHPExcel->getActiveSheet()->getStyle('F'.$cell_index.':'.'G'.$cell_index)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$cell_index, $value['goodsPrice']);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$cell_index, $value['goodsMoney']);
            $objPHPExcel->getActiveSheet()->getRowDimension($cell_index)->setRowHeight('25.5');//商品列表行高
            $cell_index++;
        }
        $cell_index--;
        //画出单元格边框
        $objPHPExcel->getActiveSheet()->getStyle('A1:G'.$cell_index)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        //设置商品头部 垂直和水平居中
        $objPHPExcel->getActiveSheet()->getStyle('A'.$goods_index.':G'.$cell_index)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A'.$goods_index.':G'.$cell_index)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        //Save Excel 2007 file 保存
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$output_info['filename'].'.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory:: createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save( 'php://output');

    }
}