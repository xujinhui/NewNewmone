<?php

namespace Home\Service;


/**
 * 商品图片导入
 */
class ImportImageService extends PSIBaseService {
    private $LOG_CATEGORY_GOODS = "基础数据-商品";

    /**
     * 商品图片导入
     *
     * @param
     *        	$params
     * @return array
     * @throws \PHPExcel_Exception
     */
    public function importGoodsFromImageFile($goodsId,$goodsName,$savename,$uploadFileGoodsPath) {
        //将数据存入数据库
        $db = M();
        $sql = "select goods_image,code from t_goods where id = '%s'";

        $goods_img = $db->query($sql,array($goodsId));
        $goods_img_info = $goods_img[0]['goods_image'];

        $goods_images = array();
        if(empty($goods_img_info) || empty(unserialize($goods_img_info))){
            $goods_images[] = array(
               'goodsName'=>$goodsName,
               'goodsPath'=>$uploadFileGoodsPath,
               'savename'=>$savename,
            );
        }else{
            $seria_goods_img_info = unserialize($goods_img_info);
            if(!$seria_goods_img_info){
                return false;
            }
            $seria_goods_img_info[] = array(
                'goodsName'=>$goodsName,
                'goodsPath'=>$uploadFileGoodsPath,
                'savename'=>$savename,
            );
            $goods_images = $seria_goods_img_info;
        }

        $install_data = serialize($goods_images);

        $sql = "update t_goods
							set goods_image = '%s'
							where id = '%s' ";
        $resule = $db->execute($sql, array($install_data,$goodsId));
        if($resule){
            //设置业务日志
            $bs = new BizlogService();
            $log = "商品图片导入：  编码 = {$goods_img[0]['code']}， 图片名称 = {$savename}";
            $bs->insertBizlog($log, $this->LOG_CATEGORY_GOODS);

            return true;
        }else{
            return false;
        }
    }

    /**
     * 商品图片删除
     * @param $goodsId
     * @param $saveName
     */
    public function GoodsImgDel($goodsId,$saveName){
        $db = M();
        $sql = "select goods_image,code from t_goods where id = '%s'";
        $goods_info = $db->query($sql,$goodsId);
        $goods_images = unserialize($goods_info[0]['goods_image']);

        $result = array();
        foreach ($goods_images as $img_key=>$images){
            if($images['savename'] == $saveName || $images['savename'] == ''){
                unset($goods_images[$img_key]);

                $sql = "update t_goods

							set goods_image = '%s'
							where id = '%s' ";
              
                $resule = $db->execute($sql, array(serialize(array_merge($goods_images)),$goodsId));
                if($resule){
                    //设置业务日志
                    $bs = new BizlogService();
                    $log = "商品图片删除：  编码 = {$goods_info[0]['code']}， 图片名称 = {$saveName}";
                    $bs->insertBizlog($log, $this->LOG_CATEGORY_GOODS);

                    unlink($images['goodsPath']);
                    $result = array(
                        "msg" => '删除成功',
                        "success" => true
                    );
                }else{
                    $result = array(
                        "msg" => '删除成功',
                        "success" => false
                    );
                }
                break;
            }
        }
        return $result;

    }

    /**
     * 商品图片信息显示
     * @param $goodsId
     */
    public function GoodsShow($goodsId){
        $db = M();
        $sql = "select goods_image from t_goods where id = '%s'";
        $goods_info = $db->query($sql,$goodsId);
        return unserialize($goods_info[0]['goods_image']);
    }


}