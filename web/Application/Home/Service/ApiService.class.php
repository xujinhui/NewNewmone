<?php
namespace Home\Service;

/**
 * Api对外接口Service
 *
 * @author 李静波
 */
class ApiService extends PSIBaseService {
    /**
     * 根据商品编号，获取某个商品详情(没有判断是否登录)
     */
    public function getGoodsInfoByCode($code){
        $db = M();

        $sql = "select  g.category_id,g.code, g.name, g.spec,   
					g.bar_code, g.memo, g.brand_id,g.open,g.unit_id,u.name unit_name
				from t_goods g,t_goods_unit u
				where code = '%s' and u.id=g.unit_id";
        $data = $db->query($sql, $code);

        $result = array();
        $result["code"] = $data[0]["code"]; //商品编码
        $result["name"] = $data[0]["name"]; //商品名称
        $result["categoryId"] = $data[0]["category_id"]; //商品分类ID
        $result["spec"] = $data[0]["spec"]; //商品规格
        $result["unitId"] = $data[0]["unit_id"]; //计量单位ID
        $result["unitName"] = $data[0]["unit_name"]; //计量单位
        $result["open"] = $data[0]["open"]; //是否启用
        $brandId = $data[0]["brand_id"];
        $result["brandId"] = $brandId; //品牌id

        //商品分类名称
        $sql = "select name from t_goods_category where id = '%s' ";
        $data = $db->query($sql, $result["categoryId"]);
        $result["categoryName"] = $data[0]['name'];

        if ($brandId) {
            $sql = "select full_name from t_goods_brand where id = '%s' ";
            $data = $db->query($sql, $brandId);
            $result["brandFullName"] = $data[0]["full_name"];
        }else{
            $result["brandFullName"] = ' ';
        }

        $result["barCode"] = $data[0]["bar_code"]; //条形码
        $result["memo"] = $data[0]["memo"]; //备注
        return $result;
    }

    /**
     * 获得销售订单的信息
     */
    public function soBillInfo($ref) {
        $db = M();

        $sql = "select s.id, s.ref, s.deal_date, s.deal_address, s.customer_id,
						c.name as customer_name, s.contact, s.tel, s.fax,
						s.org_id, o.full_name, s.biz_user_id, u.name as biz_user_name,
						s.receiving_type, s.bill_memo, s.bill_status
					from t_so_bill s, t_customer c, t_user u, t_org o
					where s.ref = '%s' and s.customer_Id = c.id
						and s.biz_user_id = u.id
						and s.org_id = o.id";
        $data = $db->query($sql, $ref);

        if ($data) {
            $v = $data[0];
            $result["ref"] = $v["ref"];
            $result["dealDate"] = $this->toYMD($v["deal_date"]);
            $result["dealAddress"] = $v["deal_address"];
            $result["customerId"] = $v["customer_id"];
            $result["customerName"] = $v["customer_name"];
            $result["contact"] = $v["contact"];
            $result["tel"] = $v["tel"];
            $result["fax"] = $v["fax"];
            $result["orgId"] = $v["org_id"];
            $result["orgFullName"] = $v["full_name"];
            $result["bizUserId"] = $v["biz_user_id"];
            $result["bizUserName"] = $v["biz_user_name"];
            $result["receivingType"] = $v["receiving_type"];
            $result["billMemo"] = $v["bill_memo"];
            $result["billStatus"] = $v["bill_status"];
            // 明细表
            $sql = "select s.id, s.goods_id, g.code, g.name, g.spec, s.goods_count, s.goods_price, s.goods_money,
					s.tax_rate, s.tax, s.money_with_tax, u.name as unit_name
				from t_so_bill_detail s, t_goods g, t_goods_unit u
				where s.sobill_id = '%s' and s.goods_id = g.id and g.unit_id = u.id
				order by s.show_order";
            $items = array();
            $data = $db->query($sql, $v['id']);

            foreach ( $data as $i => $v ) {
                $items[$i]["goodsId"] = $v["goods_id"];
                $items[$i]["goodsCode"] = $v["code"];
                $items[$i]["goodsName"] = $v["name"];
                $items[$i]["goodsSpec"] = $v["spec"];
                $items[$i]["goodsCount"] = $v["goods_count"];
                $items[$i]["goodsPrice"] = $v["goods_price"];
                $items[$i]["goodsMoney"] = $v["goods_money"];
                $items[$i]["taxRate"] = $v["tax_rate"];
                $items[$i]["tax"] = $v["tax"];
                $items[$i]["moneyWithTax"] = $v["money_with_tax"];
                $items[$i]["unitName"] = $v["unit_name"];
            }

            $result["items"] = $items;
        }
        return $result;
    }

    protected function toYMD($d) {
        return date("Y-m-d", strtotime($d));
    }
}