<?php

namespace Home\Service;

use Home\Common\FIdConst;

/**
 * 库存报表Service
 *
 * @author 李静波
 */
class InventoryReportService extends PSIBaseService {

	/**
	 * 安全库存明细表 - 数据查询
	 */
	public function safetyInventoryQueryData($params) {
		if ($this->isNotOnline()) {
			return $this->emptyResult();
		}
		
		$page = $params["page"];
		$start = $params["start"];
		$limit = $params["limit"];
		
		$result = array();
		
		$db = M();
		
		$ds = new DataOrgService();
		$rs = $ds->buildSQL(FIdConst::REPORT_SAFETY_INVENTORY, "w");
		$queryParams = array();
		
		$sql = "select w.code as warehouse_code, w.name as warehouse_name,
					g.code as goods_code, g.name as goods_name, g.spec as goods_spec,
					u.name as unit_name,
					s.safety_inventory, i.balance_count
				from t_inventory i, t_goods g, t_goods_unit u, t_goods_si s, t_warehouse w
				where i.warehouse_id = w.id and i.goods_id = g.id and g.unit_id = u.id
					and s.warehouse_id = i.warehouse_id and s.goods_id = g.id
					and s.safety_inventory > i.balance_count ";
		if ($rs) {
			$sql .= " and " . $rs[0];
			$queryParams = array_merge($queryParams, $rs[1]);
		}
		$sql .= " order by w.code, g.code
				limit %d, %d";
		$queryParams[] = $start;
		$queryParams[] = $limit;
		$data = $db->query($sql, $queryParams);
		foreach ( $data as $i => $v ) {
			$result[$i]["warehouseCode"] = $v["warehouse_code"];
			$result[$i]["warehouseName"] = $v["warehouse_name"];
			$result[$i]["goodsCode"] = $v["goods_code"];
			$result[$i]["goodsName"] = $v["goods_name"];
			$result[$i]["goodsSpec"] = $v["goods_spec"];
			$result[$i]["unitName"] = $v["unit_name"];
			$result[$i]["siCount"] = $v["safety_inventory"];
			$result[$i]["invCount"] = $v["balance_count"];
			$result[$i]["delta"] = $v["safety_inventory"] - $v["balance_count"];
		}
		
		$sql = "select count(*) as cnt
				from t_inventory i, t_goods g, t_goods_unit u, t_goods_si s, t_warehouse w
				where i.warehouse_id = w.id and i.goods_id = g.id and g.unit_id = u.id
					and s.warehouse_id = i.warehouse_id and s.goods_id = g.id
					and s.safety_inventory > i.balance_count ";
		$queryParams = array();
		if ($rs) {
			$sql .= " and " . $rs[0];
			$queryParams = array_merge($queryParams, $rs[1]);
			;
		}
		$data = $db->query($sql, $queryParams);
		$cnt = $data[0]["cnt"];
		
		return array(
				"dataList" => $result,
				"totalCount" => $cnt
		);
	}

	/**
	 * 库存超上限明细表 - 数据查询
	 */
	public function inventoryUpperQueryData($params) {
		if ($this->isNotOnline()) {
			return $this->emptyResult();
		}
		
		$page = $params["page"];
		$start = $params["start"];
		$limit = $params["limit"];
		
		$result = array();
		
		$db = M();
		$ds = new DataOrgService();
		$rs = $ds->buildSQL(FIdConst::REPORT_INVENTORY_UPPER, "w");
		
		$sql = "select w.code as warehouse_code, w.name as warehouse_name,
					g.code as goods_code, g.name as goods_name, g.spec as goods_spec,
					u.name as unit_name,
					s.inventory_upper, i.balance_count
				from t_inventory i, t_goods g, t_goods_unit u, t_goods_si s, t_warehouse w
				where i.warehouse_id = w.id and i.goods_id = g.id and g.unit_id = u.id
					and s.warehouse_id = i.warehouse_id and s.goods_id = g.id
					and s.inventory_upper < i.balance_count
					and s.inventory_upper <> 0 and s.inventory_upper is not null ";
		$queryParams = array();
		if ($rs) {
			$sql .= " and " . $rs[0];
			$queryParams = array_merge($queryParams, $rs[1]);
		}
		$sql .= " order by w.code, g.code
				limit %d, %d";
		$queryParams[] = $start;
		$queryParams[] = $limit;
		$data = $db->query($sql, $queryParams);
		foreach ( $data as $i => $v ) {
			$result[$i]["warehouseCode"] = $v["warehouse_code"];
			$result[$i]["warehouseName"] = $v["warehouse_name"];
			$result[$i]["goodsCode"] = $v["goods_code"];
			$result[$i]["goodsName"] = $v["goods_name"];
			$result[$i]["goodsSpec"] = $v["goods_spec"];
			$result[$i]["unitName"] = $v["unit_name"];
			$result[$i]["iuCount"] = $v["inventory_upper"];
			$result[$i]["invCount"] = $v["balance_count"];
			$result[$i]["delta"] = $v["balance_count"] - $v["inventory_upper"];
		}
		
		$sql = "select count(*) as cnt
				from t_inventory i, t_goods g, t_goods_unit u, t_goods_si s, t_warehouse w
				where i.warehouse_id = w.id and i.goods_id = g.id and g.unit_id = u.id
					and s.warehouse_id = i.warehouse_id and s.goods_id = g.id
					and s.inventory_upper < i.balance_count
					and s.inventory_upper <> 0 and s.inventory_upper is not null
				";
		$queryParams = array();
		if ($rs) {
			$sql .= " and " . $rs[0];
			$queryParams = array_merge($queryParams, $rs[1]);
		}
		
		$data = $db->query($sql, $queryParams);
		$cnt = $data[0]["cnt"];
		
		return array(
				"dataList" => $result,
				"totalCount" => $cnt
		);
	}

	/**
	 * 库存月报表（按仓库查询）
	 */
	public function inventoryMonthByWarehouseQueryData($params){
		if ($this->isNotOnline()) {
			return $this->emptyResult();
		}

		$page = $params["page"];
		$start = $params["start"];
		$limit = $params["limit"];

		$year = $params["year"];
		$month = $params["month"];

		$result = array();

		$bizDT = '';
		if ($month < 10) {
			$bizDT = "$year-0$month";
		} else {
			$bizDT = "$year-$month";
		}

		$sql = "select w.id, w.code, w.name
from t_warehouse w
where w.id in(
	select distinct i.warehouse_id
	from t_inventory_detail as i
	where year(i.biz_date) = $year and month(i.biz_date) = $month
	)
order by w.code
limit $start,$limit";

		$db = M();
		$warechouses = $db->query($sql,$year,$month,$start,$limit);
		foreach ($warechouses as $key=>$war){
			$result[$key]['bizDT'] = $bizDT;
			$result[$key]['warehouseCode'] = $war['code'];
			$result[$key]['warehouseName'] = $war['name'];

			$sql = "select sum(i.in_count) as in_count,sum(in_money) as in_money,sum(out_count) as out_count,sum(out_money) as out_money from t_inventory_detail as i
				where year(i.biz_date) = %d and month(i.biz_date) = %d and i.warehouse_id = '".$war['id']."'";
			$invenroty = $db->query($sql,$year,$month);
			$result[$key]['in_count'] = $invenroty[0]['in_count'];
			$result[$key]['in_money'] = $invenroty[0]['in_money'];
			$result[$key]['out_count'] = $invenroty[0]['out_count'];
			$result[$key]['out_money'] = $invenroty[0]['out_money'];

			$sql = "select sum(balance_count) as balance_count,sum(balance_money) as balance_money from t_inventory_detail as i
				where year(i.biz_date) = %d and month(i.biz_date) = %d and i.warehouse_id = '".$war['id']."' and i.ref_type='销售出库' ";
			$invenroty = $db->query($sql,$year,$month);

			$result[$key]['balance_count'] = $invenroty[0]['balance_count'];
			$result[$key]['balance_money'] = $invenroty[0]['balance_money'];
		}

		//拿到总条数
		$count  = $db->query("select count(1)
from t_warehouse w
where w.id in(
	select distinct i.warehouse_id
	from t_inventory_detail as i
	where year(i.biz_date) = $year and month(i.biz_date) = $month
	)
order by w.code");
		return array(
			"dataList" => $result, //库存报表
			"totalCount" => $count[0]['count(1)'],//总条数
		);
	}

	/**
	 * 库存月报表（查询汇总数据）
	 */
	public function inventoryMonthByentSummaryQueryData($params){
		if ($this->isNotOnline()) {
			return $this->emptyResult();
		}

		$year = $params["year"];
		$month = $params["month"];

		$result = array();

		$bizDT = '';
		if ($month < 10) {
			$bizDT = "$year-0$month";
		} else {
			$bizDT = "$year-$month";
		}
		$db = M();
		$sql = "select sum(i.in_count) as in_count,sum(in_money) as in_money,sum(out_count) as out_count,sum(out_money) as out_money,sum(balance_count) as balance_count,sum(balance_money) as balance_money from t_inventory_detail as i
				where year(i.biz_date) = %d and month(i.biz_date) = %d ";
		$query_data =  $db->query($sql,$year,$month);
		$query_data[0]['bizDT'] = $bizDT;
		return $query_data[0];

	}
}