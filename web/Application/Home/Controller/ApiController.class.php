<?php
/**
 * 外边开放API接口
 * User: cory
 * Date: 2017/2/27
 * Time: 9:45
 */
namespace Home\Controller;

use Think\Controller;
use Home\Service\ApiService;
use Home\Service\SOBillService;

class ApiController extends Controller{

    /**
     * 根据商品编码，获取商品详情
     */
    public function getGoodsByCode(){
        $code = trim($_GET['code']);
        $gs = new ApiService();
        $this->ajaxReturn($gs->getGoodsInfoByCode($code));
    }

    /**
     * 根据订单号，查看销售订单
     */
    public function getSellOrder(){
        $ref = I('ref');
        //查询订单id
        $ps = new ApiService();
        $this->ajaxReturn($ps->soBillInfo($ref));

    }

    /**
     * 新增销售订单
     */
    public function addSellOrder(){
        if(IS_POST or true){
            //$a = json_decode('{"id":"","dealDate":"2017-03-06","customerId":"071AE76F-A00A-11E6-98D5-5254001773A1","dealAddress":"\u80e1\u5fd7\u660e\u5e02\u7b2c\u516d\u90e1\u7b2c\u5341\u574a42\u865f\u8def57\u53f7","contact":"\u6797\u5e38\u4e30","tel":"","fax":"","orgId":"5EBDBE11-A129-11E4-9B6A-782BCBD7746B","bizUserId":"6C2A09CD-A129-11E4-9B6A-782BCBD7746B","receivingType":"0","billMemo":"","items":[{"goodsId":"5DFD1AFC-9F47-11E6-98D5-5254001773A1","goodsCount":10,"goodsPrice":1,"goodsMoney":10,"tax":0,"taxRate":0,"moneyWithTax":10}]}',true);
            //bug($a);

            $sell_info['id'] = I('post.id');
            $sell_info['dealDate'] = I('post.dealDate');
            $sell_info['customerId'] = I('post.customerId');
            $sell_info['dealAddress'] = I('post.dealAddress');
            $sell_info['contact'] = I('post.contact');
            $sell_info['tel'] = I('post.tel');
            $sell_info['fax'] = I('post.fax');
            $sell_info['orgId'] = I('post.orgId');
            $sell_info['bizUserId'] = I('post.bizUserId');
            $sell_info['receivingType'] = I('post.receivingType');
            $sell_info['billMemo'] = I('post.billMemo');
            $sell_info['items'][0]['goodsId'] = I('post.item.goodsId');
            $sell_info['items'][0]['goodsCount'] = I('post.item.goodsCount');
            bug($sell_info);exit;
            $json_str = json_encode($sell_info);
            $ps = new SOBillService();
            $this->ajaxReturn($ps->editSOBill($json_str));
        }
    }

}