<?php
/**
 * 打印菜单
 * User: cory
 * Date: 2016/10/20
 * Time: 11:03
 */
namespace Home\Controller;

use Think\Controller;
use Home\Service\POBillService;
use Home\Service\PWBillService;
use Home\Service\PRBillService;
use Home\Service\WSBillService;

class PrintController extends PSIBaseController {

    /**
     * 采购订单打印页面
     */
    public function pur_order(){
        $this->initVar();

        $params = array(
            "id" => I("post.bill_id")
        );
        $ps = new POBillService();
        $billInfo = $ps->poBillInfo($params);
        $sum_print = 0;
        $sum_count = 0;
        foreach ($billInfo['items'] as $info){
            $sum_print+=$info['goodsMoney'];
            $sum_count+=$info['goodsCount'];
        }
        $this->assign("billInfo", $billInfo);
        $this->assign("sum_print", sprintf("%.2f",$sum_print));
        $this->assign("sum_goods_count", sprintf("%.2f",$sum_count));
        $this->display();
    }

    /**
     * 入库单打印
     */
    public function pur_enter(){
        $this->initVar();

        $params = array(
            "id" => I("post.bill_id"),
            "pobillRef" => I("post.pobillRef")
        );

        $ps = new PWBillService();
        $billInfo = $ps->pwBillInfo($params);

        $sum_print = 0;
        foreach ($billInfo['items'] as $info){
            $sum_print+=$info['goodsMoney'];
        }

        $this->assign("billInfo", $billInfo);
        $this->assign("sum_print", sprintf("%.2f",$sum_print));

        $this->display();
    }

    /**
     * 采购退货
     */
    public function pur_refund(){
        $this->initVar();

        $params = array(
            "id" => I("post.bill_id")
        );

        $pr = new PRBillService();
        $billInfo = $pr->prBillInfo($params);
        $billInfo['id'] = $params['id'];
        $sum_print = 0;
        foreach ($billInfo['items'] as $info){
            $sum_print+=$info['goodsMoney'];
        }

        $this->assign("billInfo", $billInfo);
        $this->assign("sum_print", sprintf("%.2f",$sum_print));
        $this->display();
    }

    /**
     * 销售出库单打印
     */
    public function sell_come(){
        $this->initVar();

        $params = array(
            "id" => I("post.bill_id"),
            "sobillRef" => I("get.sobillRef")
        );

        $ws = new WSBillService();
        $billInfo = $ws->wsBillInfo($params);

        $sum_print = 0;
        foreach ($billInfo['items'] as $info){
            $sum_print+=$info['goodsMoney'];
        }

        $this->assign("billInfo", $billInfo);
        $this->assign("sum_print", sprintf("%.2f",$sum_print));
        $this->display();
    }

}
