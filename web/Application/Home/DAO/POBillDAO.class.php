<?php

namespace Home\DAO;

use Home\Common\FIdConst;

/**
 * 采购订单 DAO
 *
 * @author 李静波
 */
class POBillDAO extends PSIBaseExDAO {
	/**
	 * 采购订单执行的采购入库单信息
	 *
	 * @param array $params        	
	 * @return array
	 */
	public function poBillPWBillList($params) {
		$db = $this->db;
		
		// id: 采购订单id
		$id = $params["id"];
		
		$sql = "select p.id, p.bill_status, p.ref, p.biz_dt, u1.name as biz_user_name, u2.name as input_user_name,
					p.goods_money, w.name as warehouse_name, s.name as supplier_name,
					p.date_created, p.payment_type
				from t_pw_bill p, t_warehouse w, t_supplier s, t_user u1, t_user u2,
					t_po_pw popw
				where (popw.po_id = '%s') and (popw.pw_id = p.id)
				and (p.warehouse_id = w.id) and (p.supplier_id = s.id)
				and (p.biz_user_id = u1.id) and (p.input_user_id = u2.id)
				order by p.ref ";
		$data = $db->query($sql, $id);
		$result = array();

		foreach ( $data as $i => $v ) {
			$result[$i]["id"] = $v["id"];
			$result[$i]["ref"] = $v["ref"];
			$result[$i]["bizDate"] = $this->toYMD($v["biz_dt"]);
			$result[$i]["supplierName"] = $v["supplier_name"];
			$result[$i]["warehouseName"] = $v["warehouse_name"];
			$result[$i]["inputUserName"] = $v["input_user_name"];
			$result[$i]["bizUserName"] = $v["biz_user_name"];
			$billStatus = $v["bill_status"];
			$bs = "";
			if ($billStatus == 0) {
				$bs = "待入库";
			} else if ($billStatus == 1000) {
				$bs = "已入库";
			} else if ($billStatus == 9000) {
				$bs = "作废";
			}
			$result[$i]["billStatus"] = $bs;
			$result[$i]["amount"] = $v["goods_money"];
			$result[$i]["dateCreated"] = $v["date_created"];
			$result[$i]["paymentType"] = $v["payment_type"];
		}
		
		return $result;
	}

	/**
	 * 查询采购订单的数据，用于生成PDF文件
	 *
	 * @param array $params
	 *
	 * @return NULL|array
	 */
	public function getDataForPDF($params) {
		$db = $this->db;

		$ref = $params["ref"];

		$sql = "select p.id, p.bill_status, p.goods_money, p.tax, p.money_with_tax,
					s.name as supplier_name, p.contact, p.tel, p.fax, p.deal_address,
					p.deal_date, p.payment_type, p.bill_memo, p.date_created,
					o.full_name as org_name, u1.name as biz_user_name, u2.name as input_user_name,
					p.confirm_user_id, p.confirm_date
				from t_po_bill p, t_supplier s, t_org o, t_user u1, t_user u2
				where (p.supplier_id = s.id) and (p.org_id = o.id)
					and (p.biz_user_id = u1.id) and (p.input_user_id = u2.id) 
					and (p.ref = '%s')";

		$data = $db->query($sql, $ref);
		if (! $data) {
			return null;
		}

		$v = $data[0];
		$id = $v["id"];

		$result = array();

		$result["billStatus"] = $v["bill_status"];
		$result["supplierName"] = $v["supplier_name"];
		$result["goodsMoney"] = $v["goods_money"];
		$result["tax"] = $v["tax"];
		$result["moneyWithTax"] = $v["money_with_tax"];
		$result["dealDate"] = $this->toYMD($v["deal_date"]);
		$result["dealAddress"] = $v["deal_address"];
		$result["bizUserName"] = $v["biz_user_name"];

		$sql = "select p.id, g.code, g.name, g.spec, p.goods_count, p.goods_price, p.goods_money,
					p.tax_rate, p.tax, p.money_with_tax, u.name as unit_name
				from t_po_bill_detail p, t_goods g, t_goods_unit u
				where p.pobill_id = '%s' and p.goods_id = g.id and g.unit_id = u.id
				order by p.show_order";
		$items = array();
		$data = $db->query($sql, $id);

		foreach ( $data as $v ) {
			$item = array(
				"goodsCode" => $v["code"],
				"goodsName" => $v["name"],
				"goodsSpec" => $v["spec"],
				"goodsCount" => $v["goods_count"],
				"unitName" => $v["unit_name"],
				"goodsPrice" => $v["goods_price"],
				"goodsMoney" => $v["goods_money"]
			);

			$items[] = $item;
		}

		$result["items"] = $items;

		return $result;
	}
}