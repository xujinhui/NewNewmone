<?php

namespace Home\DAO;

use Home\Common\FIdConst;

/**
 * 采购退货出库单 DAO
 *
 * @author 李静波
 */
class PRBillDAO extends PSIBaseExDAO {

	/**
	 * 查询采购退货出库单的数据，用于生成PDF文件
	 *
	 * @param array $params
	 *
	 * @return NULL|array
	 */
	public function getDataForPDF($params) {
		$db = $this->db;

		$ref = $params["ref"];

		$sql = "select p.id, p.bill_status, w.name as warehouse_name, p.bizdt,
					p.rejection_money, u1.name as biz_user_name, u2.name as input_user_name,
					s.name as supplier_name, p.date_created, p.receiving_type
				from t_pr_bill p, t_warehouse w, t_user u1, t_user u2, t_supplier s
				where (p.warehouse_id = w.id)
					and (p.biz_user_id = u1.id)
					and (p.input_user_id = u2.id)
					and (p.supplier_id = s.id) 
					and (p.ref = '%s')";

		$data = $db->query($sql, $ref);
		if (! $data) {
			return null;
		}

		$v = $data[0];
		$id = $v["id"];

		$result = array();

		$result["billStatus"] = $v["bill_status"];
		$result["supplierName"] = $v["supplier_name"];
		$result["goodsMoney"] = $v["rejection_money"];
		$result["bizDT"] = $this->toYMD($v["biz_dt"]);
		$result["warehouseName"] = $v["warehouse_name"];
		$result["bizUserName"] = $v["biz_user_name"];

		$sql = "select g.code, g.name, g.spec, u.name as unit_name,
					p.rejection_goods_count as rej_count, p.rejection_goods_price as rej_price,
					p.rejection_money as rej_money
				from t_pr_bill_detail p, t_goods g, t_goods_unit u
				where p.goods_id = g.id and g.unit_id = u.id and p.prbill_id = '%s'
					and p.rejection_goods_count > 0
				order by p.show_order";
		$items = array();
		$data = $db->query($sql, $id);

		foreach ( $data as $v ) {
			$item = array(
				"goodsCode" => $v["code"],
				"goodsName" => $v["name"],
				"goodsSpec" => $v["spec"],
				"goodsCount" => $v["rej_count"],
				"unitName" => $v["unit_name"],
				"goodsPrice" => $v["rej_price"],
				"goodsMoney" => $v["rej_money"]
			);

			$items[] = $item;
		}

		$result["items"] = $items;

		return $result;
	}
}