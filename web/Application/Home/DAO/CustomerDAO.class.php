<?php

namespace Home\DAO;

use Home\Common\FIdConst;

/**
 * 客户资料 DAO
 *
 * @author 李静波
 */
class CustomerDAO extends PSIBaseExDAO {

	/**
	 * 获得所有的价格体系中的价格
	 */
	public function priceSystemList($params) {
		$db = $this->db;
		
		// id: 客户分类id
		$id = $params["id"];
		
		$sql = "select id, name 
				from t_price_system
				order by name";
		$data = $db->query($sql);
		
		$result = [
				[
						"id" => "-1",
						"name" => "[无]"
				]
		];
		foreach ( $data as $v ) {
			$result[] = [
					"id" => $v["id"],
					"name" => $v["name"]
			];
		}
		
		$psId = null;
		if ($id) {
			$sql = "select ps_id from t_customer_category where id = '%s' ";
			$data = $db->query($sql, $id);
			if ($data) {
				$psId = $data[0]["ps_id"];
			}
		}
		
		return [
				"psId" => $psId,
				"priceList" => $result
		];
	}
}