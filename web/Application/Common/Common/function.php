<?php
/**
 * 公共类库
 * User: cory
 * Date: 2016/10/9
 * Time: 17:34
 */
/**
 * 调试方法
 * @param $data 所有对象
 */
function bug($data,$status = 1){
    if($status == 1){
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }else{
        echo "<pre>";
        var_dump($data);
        echo "</pre>";
    }
}

/**
 * 给open字段添加禁用sql代码
 */
function getGoodsOpenSql($type = 0){
    switch ($type){
        default:
            return ' and open = 0 ';
    }
}
